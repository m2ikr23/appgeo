import { ContactoServicio } from './../../servicios/contacto.services';

import { Component } from '@angular/core';
import { NavController, NavParams,ViewController } from 'ionic-angular';
import { ModalController} from 'ionic-angular';
import { ModalContactPage } from '../modal-contact/modal-contact';
import { ModalContactSosPage } from '../modal-contact-sos/modal-contact-sos';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'page-pop-contac',
  templateUrl: 'pop-contac.html',
})
export class PopContacPage {
  pages: Array<{title: string, component: any, param: any, icon:string}>;
  language:string;

  constructor(public viewCtrl:ViewController, public modalCtrl:ModalController,
              public navCtrl: NavController, public navParams: NavParams,
              public contactoS:ContactoServicio, private translate: TranslateService) {
    
    
    let dato:any;
    this.contactoS.cargarDatos().then(info=>{
       dato = this.contactoS.obtenerContacto(); 
    });
   
    this.pages = [

      { title: 'Contáctanos', component: ModalContactPage, param: "", icon:'mail'},
      { title: 'Números y mensaje', component: ModalContactSosPage, param: dato, icon: 'create'},
    ];
  
  }

  
  public dismiss() {
    this.viewCtrl.dismiss(null);
  }

  presentModal(page,param){
    let modal = this.modalCtrl.create(page,{param});
    modal.present();
    this.dismiss();
  }

  ionViewDidEnter(){
  }

  languageSelect(lang:any){
    this.translate.use(lang);
  }
}
