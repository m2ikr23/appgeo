
import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import { NgForm } from '@angular/forms';

import { ContactoServicio } from "../../servicios/contacto.services";
import { Contacto } from "../../model/contacto";


@Component({
  selector: 'page-modal-contact-sos',
  templateUrl: 'modal-contact-sos.html',
})
export class ModalContactSosPage {
  fcontacto : Contacto;

  contacto :any;


  constructor(public viewCtrl: ViewController, public navParams: NavParams,
              public contactoS:ContactoServicio) {
                this.fcontacto = this.contactoS.obtenerContacto();

  }

 
 

  ionViewCanEnter(){
    
  }

  
  registrarContact(formulario : NgForm){
    let nombre = formulario.value.nombre;
    let telefono = formulario.value.telefono;
    let contacto1 = formulario.value.contacto1;
    let contacto2 = formulario.value.contacto2;
    let contacto3 = formulario.value.contacto3;
    let mensaje = formulario.value.mensaje;
    this.contactoS.agregarContacto(nombre,telefono,contacto1,contacto2,contacto3,mensaje);
    console.log(this.contactoS.obtenerContacto());
    this.contactoS.cargarDatos();
    console.log(this.contactoS.obtenerContacto());
    this.dismiss();
  }

  public dismiss() {
    this.viewCtrl.dismiss(null);

  }
}
