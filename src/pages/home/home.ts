import { ModalContactSosPage } from './../modal-contact-sos/modal-contact-sos';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ModalController} from 'ionic-angular';

import { SMS } from '@ionic-native/sms';
import { Geolocation } from '@ionic-native/geolocation';

import { ContactoServicio } from "../../servicios/contacto.services";
import { Contacto } from '../../model/contacto';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

import { AndroidPermissions } from '@ionic-native/android-permissions';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers:[TranslateService]

})
export class HomePage  {

  ubic = { lat: 0, lng: 0 }
  contactoList: Contacto;
  langCurrent: string;
  

  constructor(public navCtrl: NavController,public modalCtrl:ModalController, 
                public contactoS: ContactoServicio, private geo: Geolocation,
                  public androidPermissions: AndroidPermissions,public smsVar: SMS,
                  private translate: TranslateService  ) {
 
  this.permisoGeo();
  this.translate.onLangChange.subscribe((event : LangChangeEvent) =>{
    console.log('lang event', event);
  });
  }

  ionViewWillEnter(){
   
  this.ObtenerUbic();
  }

  ionViewDidEnter(){
    this.translate.onLangChange.subscribe((event : LangChangeEvent) =>{
      console.log('lang event', event);
      this.langCurrent = event.lang;
    });
    console.log('lang',this.langCurrent);
    
    
  }

  permisoGeo(){
    this .androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION)
    .then( success => console.log ( 'Permiso concedido' ), 
    error => this.androidPermissions.requestPermission( this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION) 
    ); 

    this .androidPermissions.requestPermissions ([ this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION]);
  }


  sos(){
    this.contactoS.cargarDatos().then(contacto =>{
      this.contactoList = this.contactoS.obtenerContacto();

      if (this.contactoList.nombre==""){
        this.permisoSms();
        console.log(this.contactoList);
         this.presentModal();
       }else{
        
         this.sendSMS();
       }
    });
   
 
   
  }

  presentModal() {
    let modal = this.modalCtrl.create(ModalContactSosPage,{'dato': this.contactoList});
    modal.present();
  }

  permisoSms() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.SEND_SMS)
      .then(success => console.log('Permiso concedido'),
        error => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.SEND_SMS)
      );

    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.SEND_SMS]);
  }

  ObtenerUbic() {
    this.geo.getCurrentPosition({ timeout: 10000 })
      .then(info => {
        this.ubic.lat = info.coords.latitude;
        this.ubic.lng = info.coords.longitude;
        console.log("https://maps.google.com/?q=" + this.ubic.lat + "," + this.ubic.lng);
      }).catch(error => {
        console.log("no se logro acceder a la ubicacion" + error);
      })
  }

  sendSMS() {
    let contactoNum: string[] = [];
 
    let nombre = this.contactoList.nombre;
    let telefono = this.contactoList.telefono;

      let contacto1 = this.contactoList.contacto1;
      let contacto2 = this.contactoList.contacto2;
      let contacto3 = this.contactoList.contacto3;
      let mensaje = this.contactoList.mensaje;


      if(mensaje==""){
        mensaje = "se encuentra en peligro. Contactar urgentemente.";
      }


    var options = {
      replaceLineBreaks: false,
      android: {
        intent: 'INTENT'
      }
    }
      if(contacto1!=""){
        contactoNum.push(contacto1);
      }
      if(contacto2!=""){
        contactoNum.push(contacto2);
      }
      if(contacto3!=""){
        contactoNum.push(contacto3);
      }
    
      console.log(contactoNum);
      this.sms(contactoNum, nombre, telefono,mensaje, options);
  }


  sms(contactos: string[], nombre: string, telefono: string,mensaje:string, options: {}) {
    this.smsVar.send(contactos, nombre + " " + "(" + telefono + ")" + " "+mensaje+
      ":"+" "+"Ubicación: https://maps.google.com/?q=" + this.ubic.lat + "," + this.ubic.lng
      , options)
      .then(() => {

      }, () => {
        console.log(contactos, nombre + " " + "(" + telefono + ")" + " "+mensaje+
        " "+"Ubicación: https://maps.google.com/?q=" + this.ubic.lat + "," + this.ubic.lng)
      });

  }

}
