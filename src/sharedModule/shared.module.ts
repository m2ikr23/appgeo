import { NgModule} from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { CustomHeaderComponent } from '../components/custom-header/custom-header';
import { TranslateModule } from '@ngx-translate/core'

@NgModule({
  declarations: [
    CustomHeaderComponent,
 
  ],
  imports: [
    IonicModule,
    TranslateModule
  ],
  exports: [
    CustomHeaderComponent,
  
  ]
})
export class SharedModule {}