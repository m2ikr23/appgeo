import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TabsPage } from '../pages/tabs/tabs';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { TranslateService } from '@ngx-translate/core';
import {LanguageServicio } from '../servicios/language.services';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = TabsPage;

  constructor(platform: Platform, statusBar: StatusBar,splashScreen: SplashScreen,
  				public androidPermissions:AndroidPermissions,
          private translateService: TranslateService,
          private languageService : LanguageServicio)
           {
    platform.ready().then(() => {

    this.languageService.getLang().then(lang=>{
      this.translateService.setDefaultLang(lang);
      this.translateService.use(lang);
      console.log('lang1',lang)
    });
    
    statusBar.styleDefault();
    splashScreen.hide();
  
    });
  }
  
  

}
