import { Injectable } from "@angular/core";
import { Contacto } from "../model/contacto";
import { Storage } from '@ionic/storage'

@Injectable()
export class ContactoServicio {

    contacto: Contacto;

    constructor(public storage: Storage) { }

    agregarContacto(nombre: string, telefono: string,
        contacto1: string, contacto2: string, contacto3: string,mensaje:string) {
        let contacto = new Contacto(nombre, telefono, contacto1, contacto2, contacto3,mensaje);
        this.contacto = contacto;
        if (this.storage.get('contacto') == null) {
            this.storage.set('contacto', this.contacto);
        } else {
            this.storage.remove('contacto');
            this.storage.set('contactos', this.contacto);
        }


    }

    obtenerContacto() {
    console.log(this.contacto);
    return this.contacto;  
    }

    cargarDatos() {

       return this.storage.get('contactos').then((contacto: Contacto) => {
            if (contacto == null) {
                this.contacto = new Contacto("","","","","","");
                console.log(this.contacto);
                return this.contacto;
               
            } else {
                this.contacto = contacto
                console.log(this.contacto)
                return this.contacto;
            }
        }).catch((error) => {
            console.log(error);
        });
    }
}