import { Injectable } from "@angular/core";
import { Storage } from '@ionic/storage'

@Injectable()
export class LanguageServicio {


    constructor(public storage: Storage) { }

    saveLang(lang:any) {

        if (this.storage.get('lang') == null) {
            this.storage.set('lang', lang);
        } else {
            this.storage.remove('lang');
            this.storage.set('lang', lang);
        }


    }


    getLang() {

       return this.storage.get('lang').then((lang: any) => {
            if (lang == null) {
                
                return 'en';
               
            } else {
            
                return lang;
            }
        }).catch((error) => {
            console.log(error);
        });
    }
}