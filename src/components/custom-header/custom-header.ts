
import { Component,OnInit } from '@angular/core';
import {MenuController, ModalController,ViewController,NavController} from 'ionic-angular';
import { ModalContactPage } from '../../pages/modal-contact/modal-contact';
import { ModalContactSosPage } from '../../pages/modal-contact-sos/modal-contact-sos';
import { TranslateService } from '@ngx-translate/core';
import { LanguageServicio } from '../../servicios/language.services';


@Component({
  selector: 'custom-header',
  templateUrl: 'custom-header.html',
  providers:[TranslateService]
})
export class CustomHeaderComponent  implements OnInit  {

  contactPage = false;
  contactSos = false;
  langCurrent: string [] = [];
  language  = [
    {lang:'es', name:'Español'},
    {lang:'en', name:'Inglés'},
    {lang:'pt', name:'Portugües'}
  ] ;


  ngOnInit(): void {

  
   
  }
 
  
  constructor(public navCtrl: NavController, public menuCtrl: MenuController,public modalCtrl:ModalController,
    private translate: TranslateService, public viewCtrl: ViewController,
    private languageService : LanguageServicio) {
      this.langCurrent = this.translate.getLangs();
     console.log('lang', this.langCurrent);
    

  }


 presentModal(page){ 
   if(page === 1){
     this.navCtrl.push(ModalContactPage);
   }
   if(page===  2){

    this.navCtrl.push(ModalContactSosPage);

   }
    
    setTimeout(()=>{
      
   /* let modal = this.modalCtrl.create(page);
    modal.present();
    this.dismiss();*/
    },5000)
    
  }

    public dismiss() {
    this.viewCtrl.dismiss(null);
  }
 

  toggleMenu(){
    this.menuCtrl.toggle('right');
  }

  languageSelect(lang:any){
    console.log(lang);
    this.languageService.saveLang(lang);
    this.translate.use(lang);
    this.langCurrent = this.translate.getLangs();
  }
  selectAny(lang:any){
    console.log(this.langCurrent);
      if(lang===this.langCurrent){
        return true;
      }else{
        return false;
      }
    
  }
}
